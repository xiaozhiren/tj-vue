import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

const app = createApp(App) // 创建实例
app.use(ElementPlus)
app.use(ElementPlus, { size: 'small', zIndex: 1000 });
app.mount('#app')
